# GPG-Paperstore

This project is for storing GPG secret keys on paper and recovering them. 

## Prerequisites

You need the following software:

* GnuPG
* zbarimg from zbar-tools
* qrencode
* imagemagick

To install these use the following command:

    $ sudo apt install gpg zbar-tools qrencode imagemagick

## Create paper backup of a GPG secret key

Run the following command:

    $ ./gpgpaperstore.sh <key>

`<key>` is either the GPG-ID or the email address of the key you want to store. The result is a PNG image named 
`<key>.png`.

Print the PNG image and store it in your physical archive. Mark it appropriately, so you can recognize it should you 
need to recover your GPG secret key.

## Restore a GPG secret key from paper

Scan the paper copy of your GPG secret key that you created with `gpgpaperstore`. Run this command:

    $ ./gpgpaperrecover.sh <image>

`<image>` is the image file containing the scan of the paper copy of your GPG secret key. The key will be added to your 
secret keyring.
