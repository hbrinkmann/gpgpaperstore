#!/usr/bin/env bash

TMPDIR=$(mktemp -d gpgpaperrecover_XXXXXX)

cp "$1" "${TMPDIR}/file"

(
  cd $TMPDIR

  OUTFILE=""
  zbarimg --raw file | (
    while read line
    do
      case "${line}" in
        split*)
	  OUTFILE=$(echo $line | cut -f1 -d:)
	  CONTENT=$(echo $line | cut -f2 -d:)
	  echo "${CONTENT}" > ${OUTFILE}
	  ;;
        *)
          echo "${line}" >> ${OUTFILE}
	  ;;
      esac
    done

    cat split* | base64 -d | gpg --import
  )
)

rm -r ${TMPDIR}
