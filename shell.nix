{ pkgs ? import <nixpkgs> {} }:
  pkgs.mkShell {
    # nativeBuildInputs is usually what you want -- tools you need to run
    nativeBuildInputs = with pkgs; [
      gnupg
      #helvetica-neue-lt-std
      imagemagick
      qrencode
      zbar
    ];
}
