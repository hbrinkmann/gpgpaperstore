#!/usr/bin/env bash

TMPDIR=$(mktemp -d gpgpaperstore_XXXXXX)

gpg --export-secret-keys --armor "$1" > ${TMPDIR}/file

(
  cd ${TMPDIR}

  base64 file > file.base64
  split -d -C 2500 file.base64 split
  for x in split*
  do
    ( echo -n "$x:"; cat $x ) | qrencode -o $x.png
  done

  montage -mode Concatenate -font "FreeSans" -tile 2x split*.png codes.png
)

cp ${TMPDIR}/codes.png "$1".png

rm -r ${TMPDIR}
